package reach.rest.dao;

import java.util.List;

import reach.rest.model.Model;

public interface Dao {

	public void save(Model model);
	
	public List<Model> list();
	
}