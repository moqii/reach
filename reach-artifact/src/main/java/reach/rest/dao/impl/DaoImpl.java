package reach.rest.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import reach.rest.dao.Dao;
import reach.rest.model.Model;


public class DaoImpl implements Dao {

	private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
    
	public void save(Model model) {
		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.persist(model);
		tx.commit();
		session.close();
	}

	public List<Model> list() {
		Session session = this.sessionFactory.openSession();
		List<Model> personList = session.createQuery("from Person").list();
		session.close();
		return personList;
	}

}
